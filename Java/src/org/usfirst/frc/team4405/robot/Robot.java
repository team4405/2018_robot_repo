//Keef is 'da reel beef! 2-20-18
package org.usfirst.frc.team4405.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

import java.text.DecimalFormat;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
//import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Encoder;
//import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
//import edu.wpi.first.wpilibj.Joystick.ButtonType;
import edu.wpi.first.wpilibj.Spark;
//import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
//import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.Timer;
//import edu.wpi.first.wpilibj.buttons.Button;
//import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
//import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.XboxController;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot implements PIDOutput {
	
	//NAV-X and PID stuff
	AHRS ahrs;
	PIDController turnController;
	double rotateToAngleRate;
	 
	/* The following PID Controller coefficients will need to be tuned */
	/* to match the dynamics of your drive system.  Note that the      */
	/* SmartDashboard in Test mode has support for helping you tune    */
	/* controllers by displaying a form where you can enter new P, I,  */
	/* and D constants and test the mechanism.                         */
	  
	double kP = 0.15;
	//double kP = 0.02;
	double kI = 0.00;
	double kD = 0.01;
	//double kD = 0.00;
	static final double kF = 0.00;
	
	/* This tuning parameter indicates how close to "on target" the    */
	/* PID Controller will attempt to get.                             */

	static final double kToleranceDegrees = 0.25f;
	
	//Joystick objects/variables
	Joystick left_joy_stick = new Joystick(0);
	Joystick right_joy_stick = new Joystick(1);
	Joystick elevator_joy_stick = new Joystick (2);
	XboxController xBox_controller = new XboxController(3);
	double leftjoystick_updown;
	double leftjoystick_sideside;
	double rightjoystick_updown;
	double rightjoystick_sideside;
	double elevatorjoystick_updown;
	double elevatorjoystick_sideside;
	
	//*********************PNEUMATICS**********************
	Compressor compressor= new Compressor (0);
	DoubleSolenoid box_grabber = new DoubleSolenoid(6, 7);
	
	//*********************SENSORS**********************
	//Gyro
	ADXRS450_Gyro gyro = new ADXRS450_Gyro();
	//Encoders
	Encoder leftEncoder = new Encoder(0, 1, false, Encoder.EncodingType.k4X);
	Encoder rightEncoder = new Encoder(2, 3, false, Encoder.EncodingType.k4X);
	Encoder elevatorEncoder = new Encoder(4, 5, false, Encoder.EncodingType.k4X);
	double left_wheel_distance_traveled;
	double right_wheel_distance_traveled;
	double elevator_distance_traveled;
	double current_elevator_position = 0;
	//double rotation_cf = 8.5;
	double rotation_cf = 2;
	double robot_voltage = 0;
	
	//*********************AUTON**********************
	//This is the correction factor. The bigger the number, the faster the PID function will correct. Too fast and it will overcorrect.
	//Default is 0.03
	//0.50 works well on 2017 backup robot
	double Kp = 0.50;
	//angle to be used for measuring gyro angle
	double angle = 0;
	//Drive power to use for auton
	double auton_drive_power = 0.50;
	//Rotate power to use for auton
	double auton_rotate_power = 0.50;
	//String which we store the data that tells us what side our colored near switch, scale and far switch is at
	//Format: LRL (L = Left, R = Right)
	//First character is near switch
	//Second character is scale
	//Third character is far switch
	String gameData = "";
	//For auton, tells us if the robot has made it's first rotation or not
	boolean auton_rotated = false;
	//For auton, tells us if our colored switch nearest us is our color for the match (set right before the start of auton at random)
	boolean near_switch_left = false;
	//For auton, tells us if our colored scale us is our color for the match (set right before the start of auton at random)
	boolean scale_left = false;
	//For auton, tells us if our colored switch farthest from us is our color for the match (set right before the start of auton at random)
	boolean far_switch_left = false;
	//Timer to be used to track time
	Timer timer = new Timer();
	
	//*********************2018 DRIVE**********************
	//Spark drive motors for 6 motor system
	Spark RB_2018 = new Spark(6);
	Spark LF_2018 = new Spark(8);
	Spark RF_2018 = new Spark(7);
	Spark LB_2018 = new Spark(9);
	//Define right side motors for 2018
	SpeedControllerGroup rightSideDriveMotors_2018 = new SpeedControllerGroup(RF_2018,RB_2018);
	//Define left side motors
	SpeedControllerGroup leftSideDriveMotors_2018 = new SpeedControllerGroup(LF_2018,LB_2018);
	
	//*********************2017 DRIVE**********************
	//Talon Motors definition
	WPI_TalonSRX LF = new WPI_TalonSRX(3);
	WPI_TalonSRX RF = new WPI_TalonSRX(4);
	WPI_TalonSRX LB = new WPI_TalonSRX(2);
	WPI_TalonSRX RB = new WPI_TalonSRX(5);
	//Define right side motors
	SpeedControllerGroup rightSideDriveMotors = new SpeedControllerGroup(RF,RB);
	//Define left side motors
	SpeedControllerGroup leftSideDriveMotors = new SpeedControllerGroup(LF,LB);
	
	//*********************DRIVE**********************
	//Is this the 2018 robot? default to 2017
	boolean spark_robot = true;	
	//Define drive for Gyro only autonomous Drive (default to 2017)
	DifferentialDrive robot_drive = (spark_robot) ?new DifferentialDrive(leftSideDriveMotors_2018,rightSideDriveMotors_2018) : new DifferentialDrive(leftSideDriveMotors,rightSideDriveMotors);
	double rotate_speed = .15;// = SmartDashboard.getNumber("Rotation speed (-1.0 to 1.0)", 0.25);
	
	//*********************OTHER MOTORS**********************
	Spark elevator_motor = new Spark(3);
	Spark left_intake_motor = new Spark (5);
	Spark right_intake_motor = new Spark (4); 
	double elevatorpower = 1;
	
	//*********************ELECTRICAL READINGS**********************
	PowerDistributionPanel pdp = new PowerDistributionPanel(1);
	double robotvoltage = pdp.getVoltage();
	double rightfrontmotor_amps = pdp.getCurrent(2); 
	double leftfrontmotor_amps= pdp.getCurrent(12); 
	double rightrearmotor_amps= pdp.getCurrent(3); 
	double leftrearmotor_amps= pdp.getCurrent(13); 
	double elevatormotor_amps= pdp.getCurrent(14); 
	double rightintakemotor_amps= pdp.getCurrent(0); 
	double leftintakemotor_amps = pdp.getCurrent(1); 
	
	//*********************MISC**********************
	int print_counter = 0;
	double encoder_multiplier = spark_robot==true ? 1 : 1;
	public Robot() {
	    try {
	        /* Communicate w/navX-MXP via the MXP SPI Bus.                                     */
	        /* Alternatively:  I2C.Port.kMXP, SerialPort.Port.kMXP or SerialPort.Port.kUSB     */
	        /* See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface/ for details. */
	        ahrs = new AHRS(SPI.Port.kMXP); 
	    } catch (RuntimeException ex ) {
	        DriverStation.reportError("Error instantiating navX-MXP:  " + ex.getMessage(), true);
	    }
	    turnController = new PIDController(kP, kI, kD, kF, ahrs, this);
	    turnController.setInputRange(-180.0f,  180.0f);
	    turnController.setOutputRange(-1.0, 1.0);
	    turnController.setAbsoluteTolerance(kToleranceDegrees);
	    turnController.setContinuous(true);
	    
	    /* Add the PID Controller to the Test-mode dashboard, allowing manual  */
	    /* tuning of the Turn Controller's P, I and D coefficients.            */
	    /* Typically, only the P value needs to be modified.                   */
	    LiveWindow.addActuator("DriveSystem", "RotateController", turnController);
	}
	
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		//Setting up the variables for Auton
		SmartDashboard.putNumber("Drive forward speed (-1.0 to 1.0)", 0.8);
		SmartDashboard.putString("Robot starting Location:", "LEFT");
		SmartDashboard.putString("Score a cube in center auton:", "YES");
		SmartDashboard.putString("2nd Cube in Switch?", "NO");
		SmartDashboard.putString("Stop before far Scale", "YES");
		SmartDashboard.putString("Near Switch over far Scale", "YES");
		//enable pneumatics
		compressor.setClosedLoopControl(true);
		//Initialize the values for the encoders
		leftEncoder.setMaxPeriod(.1);
		leftEncoder.setMinRate(10);
		//Calculated for 4" wheels ((4*pi)/360)
		//leftEncoder.setDistancePerPulse(0.03490658503988659153847381536977);
		leftEncoder.setDistancePerPulse(0.052359877559830);
		leftEncoder.setReverseDirection(false);
		leftEncoder.setSamplesToAverage(7);
		leftEncoder.reset();
		rightEncoder.setMaxPeriod(.1);
		rightEncoder.setMinRate(10);
		//Calculated for 4" wheels ((4*pi)/360)
		//rightEncoder.setDistancePerPulse(0.03490658503988659153847381536977);
		rightEncoder.setDistancePerPulse(0.052359877559830);
		rightEncoder.setReverseDirection(false);
		rightEncoder.setSamplesToAverage(7);
		rightEncoder.reset();
		elevatorEncoder.setMaxPeriod(.1);
		elevatorEncoder.setMinRate(10);
		//Calculated for 2.256" wheels ((2.256*pi)/360) = 0.019687313962496
		elevatorEncoder.setDistancePerPulse(0.10687313962496 * encoder_multiplier);
		if (spark_robot == true)
		{
			elevatorEncoder.setReverseDirection(false);
		}
		else
		{
			elevatorEncoder.setReverseDirection(true);
		}
		elevatorEncoder.setSamplesToAverage(7);
		elevatorEncoder.reset();
		//rip all of Elise's code
		
		
		//Solenoid
		box_grabber.set(DoubleSolenoid.Value.kOff);
		
		//Turn off the problematic safety for motors
		//Drive SW
		robot_drive.setSafetyEnabled(false);
		//2017 motors
		LF.setSafetyEnabled(false);
		LB.setSafetyEnabled(false);
		RF.setSafetyEnabled(false);
		RB.setSafetyEnabled(false);
		//2018 motors
		LF_2018.setSafetyEnabled(false);
		LB_2018.setSafetyEnabled(false);
		RF_2018.setSafetyEnabled(false);
		RB_2018.setSafetyEnabled(false);
	}

	/**
	 * This function is run once each time the robot enters autonomous mode
	 */
	@Override
	public void autonomousInit() {		
		leftEncoder.reset();
		rightEncoder.reset();
		ahrs.reset();
		gyro.reset();
				
			}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		//Get the game data, which identifies which side the near Switch, Scale and far Switch are on
		get_game_data_and_set_switch_and_scale_variables();
		//Get the user set values from the dashboard
		double drive_speed = SmartDashboard.getNumber("Drive forward speed (-1.0 to 1.0)", 0.9);
		String robot_auton_start_location = SmartDashboard.getString("Robot starting Location:", "LEFT");
		String score_a_cube_in_center_auton = SmartDashboard.getString("Score a cube in center auton:", "YES");
		String second_cube = SmartDashboard.getString("2nd Cube in Switch?", "NO");
		String stop_before_far_scale = SmartDashboard.getString("Stop before far Scale", "YES");
		String near_switch_over_far_scale = SmartDashboard.getString("Near Switch over far Scale", "YES");
		second_cube = second_cube.toUpperCase(); 
		robot_auton_start_location = robot_auton_start_location.toUpperCase();
		score_a_cube_in_center_auton = score_a_cube_in_center_auton.toUpperCase();
		stop_before_far_scale = stop_before_far_scale.toUpperCase();
		near_switch_over_far_scale = near_switch_over_far_scale.toUpperCase();
		//Make it all uppercase so as not to have case issues
		int rotation_direction = robot_auton_start_location.matches("LEFT") ? 1 : -1;
		int delay_in_ms = 0;
		rotate_speed = .10;
		double shootingpower = 0.8;
		double side_specific_adjustment = DriverStation.getInstance().getAlliance() == DriverStation.Alliance.Blue ? 0: 0; 

		//CURVE DRIVE CODE
		//auton_curve_drive(235, 0.55, elevator_motor, elevatorEncoder, false, 190, elevatorpower, true, (16 * rotation_direction));	
		//robot_pause (1000000, elevator_motor, elevatorEncoder, elevatorpower, 190, true);

/**
 * CENTER AUTON: SCORE 2 cubes, grab a 3rd
 */
		if (robot_auton_start_location.matches("CENTER") && (score_a_cube_in_center_auton.matches("YES")))
		{
			System.out.println("I am in CENTER Auton scoring 2 cubes");
			//Set up PID values
			kP = 0.07;
			kI = 0.00;
			kD = 0.05;
			turnController = new PIDController(kP, kI, kD, kF, ahrs, this);
			turnController.setInputRange(-180.0f,  180.0f);
			turnController.setOutputRange(-1.0, 1.0);
			turnController.setAbsoluteTolerance(kToleranceDegrees);
			turnController.setContinuous(true);
			
			//Set rotation direction based on the switch
			rotation_direction = near_switch_left ? 1 : -1;
			//Drive straight to that I am not touching the wall so I can rotate
			auton_drive (5 + side_specific_adjustment, drive_speed, 80, (0 * rotation_direction));
			
			//Drive diagonally toward the switch
			if (near_switch_left == false) 
			{
				auton_drive (80 + (rotation_direction*10), drive_speed, 80, (-40 * rotation_direction));
			}
			else
			{
				auton_drive (80 + (rotation_direction*10), drive_speed, 80, (-45 * rotation_direction));
			}
			robot_pause (250, 80, true);
			
			//Drive directly at the switch
			auton_drive(40 + (rotation_direction * -5), drive_speed, 80, (0 * rotation_direction));
			auton_drive(15, drive_speed, 80, (0 * rotation_direction));
			robot_pause (100, 80);
			
			//score the first cube
			cube_expel(shootingpower);
				
			robot_pause (100, 80);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			
			//Back up
			auton_drive(-33, -drive_speed, 80, 0);
			
			//drive backwards toward the front of the pyramid 
			auton_drive(-85, -drive_speed, 0, -55 * rotation_direction);
			
			//drive forward to get the second cube
			auton_drive(47.5, drive_speed, 0, 0);
			robot_pause (delay_in_ms, 0);
			
			cube_intake(1);
			
			robot_pause (300, 0);
			
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			
			//backup
			auton_drive(-5, -drive_speed, 0,  0);
			
			//rotate toward vault
			turn_robot ((-110), 0);
			
			//drive in front of vault
			auton_drive(30, drive_speed, 0, -110);
			robot_pause (100, 0);

			//drive straight at vault
			auton_drive(30, drive_speed,  0, -180);
			robot_pause (100, 0);
			
			//shoot cube, leave arms closed
			left_intake_motor.set(-1);
			right_intake_motor.set(-1);
			
			robot_pause (250);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			
			//3rd cube

			//Open arms
			cube_expel(0);
			//turn around
			turn_robot ((27.028), 0);
			//go to 3rd cube
			auton_drive(62.036, drive_speed, 0, 27.028);
			//Close arms
			cube_intake(1);
			robot_pause (500, 0);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			//turn around
			turn_robot (-152.972, 0);
			//go to 3rd cube
			auton_drive(57.036,drive_speed, 0, -152.972);
			//shoot 3rd cube
			left_intake_motor.set(-1);
			right_intake_motor.set(-1);
			robot_pause (250);
			//turn off motors
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			
		}
/**
 *  CROSS THE LINE AUTON
 */
		else if ( robot_auton_start_location.matches("CENTER") || robot_auton_start_location.length() < 4 )
		{
			System.out.println("I_am_in_center_auton");		
			//(double desired_distance, double robot_drive_power, Spark secondary_motor, Encoder secondary_motor_encoder, boolean clear_secondary_motor_encoder, double distance_to_move_secondary_motor, double secondary_motor_power )
			auton_drive(97.75 + side_specific_adjustment, drive_speed,30, 0);
		}
/**
 *  NEAR SCALE AUTON
 */
		
/************************************************************PRACTICE CODE TO TEST LEFT SCALE AUTON!!!!!!!!!!!!!!!!!!!!!!!!!!************************************************************/
		else if ((robot_auton_start_location.matches("LEFT") && scale_left == true) || (robot_auton_start_location.matches("RIGHT") && scale_left == false))
		//else if ((robot_auton_start_location.matches("LEFT")) || (robot_auton_start_location.matches("RIGHT") && scale_left == false))
		{			
			System.out.println("I_am_in_scale_auton");		

			//hook in towards scale
			auton_drive(100 + (side_specific_adjustment*0.4), 0.95, 190, (0 * rotation_direction));
			auton_drive(25, 0.95, 190, (4 * rotation_direction));
			auton_drive(25, 0.9,  190, (10 * rotation_direction));
			auton_drive(25, 0.85, 190, (14 * rotation_direction));
			auton_drive(15, 0.8,  190, (16 * rotation_direction));
			auton_drive(15, 0.75, 190, (18 * rotation_direction));
			auton_drive(20, 0.75,  190, (22 * rotation_direction));
			auton_drive(25, 0.75,  190, (24 * rotation_direction));
			//wait for the arms to get high enough
			//robot_pause (1000, 190, true);
			//drive over scale once arms are high enough
			auton_drive(13, 0.7, 190, (26 * rotation_direction));
			//drive over scale more
			auton_drive(5, 0.7, 190, (26 * rotation_direction));
			//shoot cube
			cube_expel(0.6);
			robot_drive.tankDrive(-0.1, -0.1);
			Timer.delay(0.2); 
			
			robot_pause (50);
			
			robot_pause (250);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
	
			//Back up
			auton_drive(-20, -drive_speed, 190, (16 * rotation_direction));
			robot_pause (1000, 0);
			
			//turn around toward second cube while lowering arms
			turn_robot (((165 * rotation_direction) - side_specific_adjustment ), 0);
			robot_pause (200, 0);

			//drive toward second cube
			auton_drive(20 + (side_specific_adjustment*4), 1, 0, ((165 * rotation_direction) - side_specific_adjustment));
			auton_drive(19 + (side_specific_adjustment*4), 0.85, 0, ((165 * rotation_direction) - side_specific_adjustment));

			//get second cube
			cube_intake(1);
			
			robot_pause (500, 0);
		
			//Score second cube in switch
			if(second_cube.equals("YES") && ((robot_auton_start_location.matches("LEFT") && near_switch_left == true) || (robot_auton_start_location.matches("RIGHT") && near_switch_left == false)))
			{
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				//back up a little bit so cube doesnt hit the lip of the switch
				auton_drive(-3, -drive_speed, 80, (155 * rotation_direction));
				//wait for arms to rise
				robot_pause (750, 80, true);
				//drive over switch
				auton_drive(13, drive_speed, 80, (155 * rotation_direction));
				robot_pause (delay_in_ms);
				//shoot cube into switch
				cube_expel(shootingpower);
				
				robot_pause (delay_in_ms/2);
				left_intake_motor.set(0);
				right_intake_motor.set(0);

				robot_pause (500);
				//back up
				auton_drive(-20, -drive_speed, 90, 0);
				
			}
			//second cube in Scale
			else 
			{	
				//back up and rise arms
				auton_drive(-20, -drive_speed,195, (180 * rotation_direction));
				//turn motors off
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				//turn toward scale and rise arms
				turn_robot ((5 * rotation_direction), 195);
				//wait for arms to rise
				robot_pause (700, 195, true);
				//drive straight to avoid going on platform and getting tippy
				auton_drive(35, drive_speed, 195, (5 * rotation_direction));
				//turn in so make sure we score cube for last little bit
				auton_drive(5, drive_speed, 195, (35.38 * rotation_direction));
				robot_pause (delay_in_ms);
				//shoot cube
				cube_expel(0.25);
				
				robot_pause (500);
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				robot_pause (50);
				//backup
				auton_drive(-35, -drive_speed, 190, 0);
				robot_pause (2500, 0);

			}
			
		}
		
		/**
		 * AUTON STATE: CLOSE SWITCH
		 */
		else if ((( robot_auton_start_location.matches("LEFT") && near_switch_left == true) || ( robot_auton_start_location.matches("RIGHT") && near_switch_left == false)) && (near_switch_over_far_scale.matches("YES"))) 
		{
			System.out.println("I_am_in_close_switch_auton");		

			//drive to the side of the cube and raise arms
			auton_drive(113.5 + side_specific_adjustment, drive_speed, 80, 0);
			robot_pause (100, 80);
			//rotate toward cube
			turn_robot ((90 * rotation_direction), 80);
			
			//drive over switch
			auton_drive(23.5, drive_speed, 80, (90 * rotation_direction));
			
			robot_pause (50, 80);
			//shoot cube
			cube_expel(shootingpower);
			robot_pause (250);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			//Back up
			auton_drive(-22, -drive_speed, 80, (90 * rotation_direction));
			//rotate away from driver station and lower arms
			turn_robot ((0 * rotation_direction), 0);
			//drive away from driver station and lower arms
			auton_drive(75, drive_speed, 0, (0 * rotation_direction));
			//rotate toward the center of the field
			turn_robot ((90 * rotation_direction), 0);
			// drive to get in front of the second cube
			auton_drive(20, drive_speed, 0, (90 * rotation_direction));
			//rotate toward the cube
			turn_robot ((165 * rotation_direction), 0);
			//drive toward the cube
			auton_drive(45, drive_speed, 0, (165 * rotation_direction));
			//pick up the cube
			cube_intake (shootingpower);
			
			robot_pause (200, 80);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			robot_pause (200, 0);
			//raise up the arms
			robot_pause (1000, 80, true);
			//drive over the switch
			auton_drive(15, drive_speed, 80, (152.5 * rotation_direction));
			robot_pause (50, 80);
			//shoot the cube
			cube_expel(shootingpower);
			robot_pause (200);
			left_intake_motor.set(0);
			right_intake_motor.set(0);
			//back up
			auton_drive(-20, -drive_speed, 80, false, (180 * rotation_direction));
		}		

		/**
		 * FAR SCALE AUTON
		 */
		else if ((robot_auton_start_location.matches("LEFT") && near_switch_left == false) || (robot_auton_start_location.matches("RIGHT") && near_switch_left == true))
		{
			System.out.println("I_am_in_far_scale_auton");		

			//drive in between the switch and scale
			auton_drive(190 + side_specific_adjustment, drive_speed, 50, false, 0);

			//turn toward the center
			turn_robot ((90 * rotation_direction),  50);

			//drive across the field and lift the elevator
			//auton_drive(135.25, drive_speed, 20, (90 * rotation_direction));
			if (stop_before_far_scale.matches("YES"))
			{
				auton_drive(135.25, drive_speed, 100,  (90 * rotation_direction));
			}
				
			else if (!stop_before_far_scale.matches("YES"))
			{
				auton_drive(135.25, drive_speed, 100,  (90 * rotation_direction));
				auton_drive(25, drive_speed, 130,  (90 * rotation_direction));
				auton_drive(20, 0.75, 140, (90 * rotation_direction));
				robot_pause (50,150);
			
				turn_robot ((0 * rotation_direction), 150);
				//robot_pause (delay_in_ms, 190);
				
				auton_drive(40, .70, 190,  (0 * rotation_direction));
				
				robot_pause (200, 190);
	
				cube_expel(.75);
				
				robot_pause (delay_in_ms/2);
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				//Back up
				auton_drive(-20, -.65,  190, (0 * rotation_direction)); 
	
				/*
				//wait for auton to end and lift elevator a bit higher
				robot_pause (15000, 80);
				
				
				turn_robot ((180 * rotation_direction), 80);
				//robot_pause (delay_in_ms);
	
				auton_drive(17.75, drive_speed, 80, (180 * rotation_direction));
				
				robot_pause (200);
	
				cube_expel(shootingpower);
				
				robot_pause (delay_in_ms/2);
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				
				robot_pause (200);
				
				auton_drive(-17.75, -drive_speed, 80, (180 * rotation_direction));
				
	
				System.out.println("I_am_in_far_scale_auton");		
	
				auton_drive(195, drive_speed, 190, true, 0);
				//robot_pause (delay_in_ms);
				
				turn_robot ((90 * rotation_direction), 190);
				//robot_pause (delay_in_ms);
				
				auton_drive(160.25, drive_speed, 190,  (90 * rotation_direction));
				auton_drive(20, 0.6, 190, (90 * rotation_direction));
				//robot_pause (delay_in_ms);
				
				turn_robot ((-90 * rotation_direction), 190);
				//robot_pause (delay_in_ms, 190);
				
				auton_drive(-20, .55, 190,  (-90 * rotation_direction));
				
				robot_pause (delay_in_ms/2, 190);
	
				cube_expel(shootingpower);
				
				robot_pause (delay_in_ms/2);
				left_intake_motor.set(0);
				right_intake_motor.set(0);
				//Back up
				auton_drive(-20, -.65,  190, false, (-90 * rotation_direction)); */
			}
		}


		/**
 * UNKNOWN AUTON (dont do anything)
 */
		else 
		{
			System.out.println("I_am_lost");	
			//Just wait and do nothing until auton is over
			robot_pause (60000);
		}
		//Kill the motors
		left_intake_motor.set(0);
		right_intake_motor.set(0);
		//Wait forever at the end, but keep the elevator at the same level
		robot_pause (9999999);
	}
	
	//Gets the game data, will wait until it gets the data
	public void get_game_data_and_set_switch_and_scale_variables()
	{
		//Get the game data from the field.
		//Format: LRL where it is 3 letters, each of the 3 letters can be either L or R meaning Left or Right
		//The first character  
		while(gameData.length() < 2){
			gameData = DriverStation.getInstance().getGameSpecificMessage();
		}
		//print the results of the field setup
		System.out.println("The field is setup as (1st letter is near switch, 2nd letter in scale, 3rd letter is far switch): " + gameData);
		if(gameData.charAt(0) == 'L' || gameData.charAt(0) == 'l'){	
			near_switch_left = true;
		} else { 
			near_switch_left = false;
		}
		if(gameData.charAt(1) == 'L' || gameData.charAt(1) == 'l'){
			 scale_left = true;
		} else {
			scale_left = false;
		}
		if(gameData.charAt(2) == 'L' || gameData.charAt(2) == 'l'){
			far_switch_left = true;
		} else {
			far_switch_left = false;
		}
		//Publish this to the dashboard
		//SmartDashboard.putString("Near switch: ", near_switch_left ? "Left" : "Right");
		//SmartDashboard.putString("Scale: ", scale_left ? "Left" : "Right");
		//SmartDashboard.putString("Far switch: ", far_switch_left ? "Left" : "Right");
	}
	
	/**
	 * OVERLOAD
	 * This version does specify a location to move the elevator, so it holds in current position
	 */
	public void turn_robot(double desired_angle) {
		turn_robot(desired_angle, elevatorEncoder.getDistance());
	}

	public void turn_robot(double desired_angle, double elevator_hold_position) {
		//Cuts down on the prints
		int print_counter = 0;
		long start_time = System.currentTimeMillis();
		//Get the angle
    	angle = ahrs.getAngle();
		double total_turn = Math.abs(desired_angle - angle);
		
		//reset the gyro before starting
		//gyro.reset();
		
		//Set the PID
		 turnController.setSetpoint(desired_angle);
		 turnController.enable();
		
		//Get the absolute value of the  desired angle once
		double abs_desired_corrected_angle = (desired_angle) - rotation_cf;
    	double baseline_power = 0.85;
		double desired_power = baseline_power + rotate_speed;
    	
		while (isAutonomous() && isEnabled() && Math.abs(abs_desired_corrected_angle - angle) > 1 || 1200 > (System.currentTimeMillis()- start_time) ) 
		{
        	//Get the angle
        	angle = ahrs.getAngle(); 
        	//calculate the desired power (slowing down when you get near the end of the rotation)
        	
        		desired_power = baseline_power + (Math.abs((abs_desired_corrected_angle - angle)/total_turn) * rotate_speed);
        	  
        	if (angle < abs_desired_corrected_angle) 
        	{
	        	//Set the drive speed (auton_drive_power) and the angle (desired_angle + (current angle of the robot * the drive power * Kp [correction factor]))
	    		robot_drive.tankDrive(desired_power , -desired_power ); // drive towards desired_distance
        		//robot_drive.curvatureDrive(0, rotateToAngleRate, true);
        	}
        	
        	else if ( angle > abs_desired_corrected_angle) 
        	{
        		//Set the drive speed (auton_drive_power) and the angle (desired_angle + (current angle of the robot * the drive power * Kp [correction factor]))
        		robot_drive.tankDrive(-desired_power , desired_power ); // drive towards desired_distance
        		//robot_drive.curvatureDrive(0, rotateToAngleRate, true);
        	}
        	else if (Math.abs(angle - abs_desired_corrected_angle) < kToleranceDegrees)
        	{
        		robot_drive.tankDrive(0, 0);
        	}
        		
    		//print the angle every fiftieth iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Angle while turning: %f\n", angle);
            	System.out.printf("Total turn: %f\n", total_turn);
            	System.out.printf("Desired Angle: %f\n", desired_angle);
            	System.out.printf("Right Encoder while running: %f\n", right_wheel_distance_traveled);
            	System.out.printf("Left Encoder while running: %f\n", left_wheel_distance_traveled);
            }
            
			//Function to put the motor in the right position
        	go_to_and_hold_elevator(elevator_hold_position);
        	//print the secondary motor encoder every tenth iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Elevator Motor Encoder while turning: %f\n", elevatorEncoder.getDistance());
            }
            
            //Delay to slow down so that it does not suck up 100% of CPU time
            //Timer.delay(0.004);
            //increment the print counter
            print_counter++;
        }
		//kill the motors
		robot_drive.tankDrive(0, 0);
	}

	/**
	 * OVERLOAD
	 * This version does hold the secondary motor at its current position
	 */
	public void robot_pause(double desired_time_in_ms) 
	{
		robot_pause(desired_time_in_ms, elevatorEncoder.getDistance(),false);
	}
	
	/**
	 * OVERLOAD
	 * This version does hold the secondary motor at desired position, but will not exit on elevator
	 */
	public void robot_pause(double desired_time_in_ms, double elevator_hold_position)
	{
		robot_pause(desired_time_in_ms, elevator_hold_position, false);
	}
	/**
	 * Stops the robot for a specified amount of time. If non-null secondary motor and encoder are passed, it will go to and hold the position desired of the secondary motor
	 */
	public void robot_pause(double desired_time_in_ms, double elevator_hold_position, boolean exit_on_elevator) {
		//first thing we do is pause
		robot_drive.arcadeDrive(0,0);
		
		double start_time = System.currentTimeMillis();
		//pause the robot until time delay ends, keep motor in the current position if so desired
	
		//while (isAutonomous() && isEnabled() && desired_time_in_ms > (System.currentTimeMillis()- start_time) && (secondary_motor_encoder.getDistance() <= secondary_encoder_hold_position && exit_on_elevator == true))
		while (isAutonomous() && isEnabled() && desired_time_in_ms > (System.currentTimeMillis()- start_time) && (elevatorEncoder.getDistance() < elevator_hold_position || exit_on_elevator == false))
		//while (isAutonomous() && isEnabled() && elevatorEncoder.getDistance() < elevator_hold_position)
		{
			robot_drive.arcadeDrive(0,0); 

			go_to_and_hold_elevator(elevator_hold_position);
			//Delay to slow down so that it does not suck up 100% of CPU time
			Timer.delay(0.004);        
        }
	}
	
	public void go_to_and_hold_elevator(double hold_position)
	{
		double actualposition = elevatorEncoder.getDistance();
		//in between 1 inch of the hold position
		if (actualposition < hold_position + 1 && actualposition > hold_position - 1)
		{
			elevator_motor.set(0.0);
		}	
		else if (actualposition < hold_position)
		{ 
			elevator_motor.set(elevatorpower);
		}
		else if (actualposition > hold_position)
		{ 
			elevator_motor.set(-elevatorpower);
		}
	}
	
	/**
	 * OVERLOAD
	 * This version holds the motor in position and assumes to ignore the ramp up
	 */
	public void auton_drive(double desired_distance, double robot_drive_power, double desired_angle) {
		auton_drive(desired_distance, robot_drive_power, elevatorEncoder.getDistance(), true, desired_angle);
	}
	/**
	 * OVERLOAD
	 * This version assumes to ignore the ramp up
	 */
	public void auton_drive(double desired_distance, double robot_drive_power, double elevator_hold_position, double desired_angle ) {
		auton_drive( desired_distance, robot_drive_power, elevator_hold_position, true, desired_angle);
	}
	
	/**
	 * Function that will drive at a desired angle (using a gyro for correction) a specified distance at a specified power
	 * Function will also move a elevator motor while driving forward a specified distance according to elevator encoder value
	 */
	public void auton_drive(double desired_distance, double robot_drive_power, double elevator_hold_position, boolean ignore_ramp_up, double desired_angle )  {
		//Cuts down on the prints
		int print_counter = 0;
		double start_time = System.currentTimeMillis();
		double watch_dog_timer = 750 + Math.abs(50*(desired_distance/robot_drive_power)); 
		
		//Set the PID
		 turnController.setSetpoint(desired_angle - rotation_cf);
		 turnController.enable();
		
		//reset the encoder before starting
		leftEncoder.reset();
		rightEncoder.reset();
		//Get the distances each wheel has traveled
		left_wheel_distance_traveled = leftEncoder.getDistance();
		right_wheel_distance_traveled = rightEncoder.getDistance();
    	
		//reset the gyro before starting
		//gyro.reset();
        //Get the angle
    	angle = ahrs.getAngle();
    	
		//Get the start time for the power ramp up
		long starttime = System.currentTimeMillis();
		//stores the elapsed time (System.currentTimeMillis() - starttime)
		long elapsedtime = 0;
		//Calculated ramp power. Will go to full robot_drive_power after ramp time completes
		double ramppower = 0;
		 
		while (isAutonomous() && isEnabled() && Math.abs(left_wheel_distance_traveled) < Math.abs(desired_distance) && Math.abs(right_wheel_distance_traveled) < Math.abs(desired_distance) && watch_dog_timer > (System.currentTimeMillis() - start_time)) {
        	//Get the angle
        	angle = ahrs.getAngle(); 
        	
        	//Get the distances each wheel has traveled
    		left_wheel_distance_traveled = leftEncoder.getDistance();
    		right_wheel_distance_traveled = rightEncoder.getDistance();
    		
    		elapsedtime = System.currentTimeMillis()-starttime; 
    		if(elapsedtime < 250 && !ignore_ramp_up) 
    		{
    			ramppower = elapsedtime*0.004*robot_drive_power; 
    		}
    		
    		else if(elapsedtime >= 250 || ignore_ramp_up)
    		{
    			ramppower = robot_drive_power; 
    		}
            //Set the drive speed (auton_drive_power) and the angle (desired_angle + (current angle of the robot * the drive power * Kp [correction factor]))
    		//robot_drive.arcadeDrive(ramppower, ( (desired_angle - angle) * (.25*ramppower))); // drive towards desired_distance
    		//if on target, go straight
    		if(turnController.onTarget())
    		{
    			robot_drive.arcadeDrive(ramppower, 0); 
    		}
    		//otherwise turn
    		else
    		{
    			robot_drive.arcadeDrive(ramppower, rotateToAngleRate);
    		}
    		
    		//robot_drive.arcadeDrive(auton_drive_power, 0); // drive towards heading 0
            
    		//print the angle every 50th iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Angle while running: %f\n", angle);
            	System.out.printf("Right Encoder while running: %f\n", right_wheel_distance_traveled);
            	System.out.printf("Left Encoder while running: %f\n", left_wheel_distance_traveled);
            }
            
            //Function to put the motor in the right position
			go_to_and_hold_elevator(elevator_hold_position);
        	//print the secondary motor encoder every tenth iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Secondary Motor Encoder while auton driving: %f\n", elevatorEncoder.getDistance());
            }
            
            //Delay to slow down so that it does not suck up 100% of CPU time
            Timer.delay(0.004);
            //increment the print counter
            print_counter++;
        }//end while loop
		
		//Stop the secondary motor at the end no matter what (to be safe) 
		elevator_motor.set(0);
	}
	public void auton_curve_drive(double desired_distance, double robot_drive_power, double evelator_hold_position, boolean ignore_ramp_up, double desired_angle )  {
		//Cuts down on the prints
		int print_counter = 0;
		
		//Set the PID
		 turnController.setSetpoint(gyro.getAngle());
		 turnController.enable();
		
		//reset the encoder before starting
		leftEncoder.reset();
		rightEncoder.reset();
		//Get the distances each wheel has traveled
		left_wheel_distance_traveled = leftEncoder.getDistance();
		right_wheel_distance_traveled = rightEncoder.getDistance();
    	
		//reset the gyro before starting
		//gyro.reset();
        //Get the angle
    	angle = ahrs.getAngle();
    	
		//Get the start time for the power ramp up
		long starttime = System.currentTimeMillis();
		//stores the elapsed time (System.currentTimeMillis() - starttime)
		long elapsedtime = 0;
		//Calculated ramp power. Will go to full robot_drive_power after ramp time completes
		double ramppower = 0;
		 
		while (isAutonomous() && isEnabled() && Math.abs(left_wheel_distance_traveled) < Math.abs(desired_distance) && Math.abs(right_wheel_distance_traveled) < Math.abs(desired_distance)) {
        	
			//Get the distances each wheel has traveled
    		left_wheel_distance_traveled = leftEncoder.getDistance();
    		right_wheel_distance_traveled = rightEncoder.getDistance();
			turnController.setSetpoint(desired_angle/(desired_angle/Math.abs(left_wheel_distance_traveled > right_wheel_distance_traveled ? left_wheel_distance_traveled : right_wheel_distance_traveled )));

			//Get the angle
        	angle = ahrs.getAngle(); 

    		elapsedtime = System.currentTimeMillis()-starttime; 
    		if(elapsedtime < 250 && !ignore_ramp_up) 
    		{
    			ramppower = elapsedtime*0.004*robot_drive_power; 
    		}
    		
    		else if(elapsedtime >= 250 || ignore_ramp_up)
    		{
    			ramppower = robot_drive_power; 
    		}
            //Set the drive speed (auton_drive_power) and the angle (desired_angle + (current angle of the robot * the drive power * Kp [correction factor]))
    		//robot_drive.arcadeDrive(ramppower, ( (desired_angle - angle) * (.25*ramppower))); // drive towards desired_distance
    		//if on target, go straight
    		if(turnController.onTarget())
    		{
    			robot_drive.arcadeDrive(ramppower, 0); 
    		}
    		//otherwise turn
    		else
    		{
    			robot_drive.arcadeDrive(ramppower, rotateToAngleRate);
    		}
    		
    		//robot_drive.arcadeDrive(auton_drive_power, 0); // drive towards heading 0
            
    		//print the angle every 50th iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Angle while running: %f\n", angle);
            	System.out.printf("Right Encoder while running: %f\n", right_wheel_distance_traveled);
            	System.out.printf("Left Encoder while running: %f\n", left_wheel_distance_traveled);
            }

			//Function to put the motor in the right position
			go_to_and_hold_elevator(evelator_hold_position);
        	//print the secondary motor encoder every tenth iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Secondary Motor Encoder while running: %f\n", elevatorEncoder.getDistance());
            }
            //Delay to slow down so that it does not suck up 100% of CPU time
            Timer.delay(0.004);
            //increment the print counter
            print_counter++;
        }//end while loop
		
		//Stop the secondary motor at the end no matter what (to be safe)
		elevator_motor.set(0);
	}

	public void cube_expel(double shootingpower) {
	
		if(isAutonomous() && isEnabled())
		{
			box_grabber.set(DoubleSolenoid.Value.kReverse);
			left_intake_motor.set(-shootingpower);
			right_intake_motor.set(-shootingpower);
		}
			
		while (isAutonomous() && isEnabled() && box_grabber.get()!=DoubleSolenoid.Value.kReverse) {
			box_grabber.set(DoubleSolenoid.Value.kReverse);
			Timer.delay(0.004); 
		}
	}
	
	public void cube_intake(double shootingpower) { 
	
		if(isAutonomous() && isEnabled())
		{
			box_grabber.set(DoubleSolenoid.Value.kForward);
			left_intake_motor.set(shootingpower);
			right_intake_motor.set(shootingpower);
		}
		
		while (isAutonomous() && isEnabled() && box_grabber.get()!=DoubleSolenoid.Value.kForward) {
			box_grabber.set(DoubleSolenoid.Value.kForward);
			Timer.delay(0.004); 
		}	
	}
		
	/**
	 * This function is called once each time the robot enters tele-operated
	 * mode
	 */
	@Override
	public void teleopInit() {
		
		leftEncoder.reset();
		rightEncoder.reset();
		//stop all drive motors
		robot_drive.tankDrive(0, 0);
		//Pneumatic solenoid
		//box_grabber.set(DoubleSolenoid.Value.kOff);
		//enable pneumatics
		//compressor.setClosedLoopControl(true);
		//get the current elevator position before we start teleOp
		current_elevator_position = elevatorEncoder.getDistance();		
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {

		//Get encoder values
		while (!isEnabled())
		{
			robot_drive.tankDrive (0, 0);
			elevator_motor.set(0);
			left_intake_motor.set(0);
			right_intake_motor.set(0); 
		}
		left_wheel_distance_traveled = leftEncoder.getDistance();
		right_wheel_distance_traveled = rightEncoder.getDistance();
		elevator_distance_traveled = elevatorEncoder.getDistance();
		current_elevator_position = elevatorEncoder.getDistance();
    	angle = ahrs.getAngle();
		SmartDashboard.putNumber("left encoder", left_wheel_distance_traveled);
		SmartDashboard.putNumber("right encoder", right_wheel_distance_traveled);
		SmartDashboard.putNumber("elevator encoder", elevator_distance_traveled);
		SmartDashboard.putNumber("gyro", angle);
		//Get Joystick values
		leftjoystick_updown = left_joy_stick.getY();
		leftjoystick_sideside = left_joy_stick.getX();
		rightjoystick_updown = right_joy_stick.getY();
		rightjoystick_sideside = right_joy_stick.getX();
		elevatorjoystick_updown = elevator_joy_stick.getY();
		elevatorjoystick_sideside = elevator_joy_stick.getX();
		leftjoystick_sideside = left_joy_stick.getX();
		
		//leftjoystick_updown = xBox_controller.getY(Hand.kLeft);
		//rightjoystick_updown = xBox_controller.getY(Hand.kRight);
		
		//robot_drive.arcadeDrive(-leftjoystick_updown, rightjoystick_updown); // drive towards desired_distance

		//Set elevator motor                   //Deadzone
		if(Math.abs(elevatorjoystick_updown) > 0.05)
		{
			elevator_motor.set(elevatorjoystick_updown); 
		}
		else
		{
			//During the first 3 seconds, hold the elevator so it does not fall and run into the switch/scale after auton
			if(Timer.getMatchTime() > 132)
			{
				go_to_and_hold_elevator (current_elevator_position);
			}
			else
			{
				elevator_motor.set(0); 
			}	
		}
		
		if (right_joy_stick.getRawButton(5)== true || right_joy_stick.getRawButton(3)== true)
		{
			robot_drive.tankDrive(-leftjoystick_updown * .5, -rightjoystick_updown * .5); 
		}
		else
		{
			robot_drive.tankDrive(-leftjoystick_updown, -rightjoystick_updown);	
		}
		
		if(right_joy_stick.getRawButton(1)==true)
		{
			left_intake_motor.set(1);
			right_intake_motor.set(1);
			box_grabber.set(DoubleSolenoid.Value.kForward);
			//get the current elevator position before we start teleOp
			current_elevator_position = elevatorEncoder.getDistance();
		}
		else if(right_joy_stick.getRawButton(2)==true)
		{
			left_intake_motor.set(-.5);
			right_intake_motor.set(-.5);
			box_grabber.set(DoubleSolenoid.Value.kReverse);
			//get the current elevator position before we start teleOp
			current_elevator_position = elevatorEncoder.getDistance();
		}
		
		else if(left_joy_stick.getRawButton(1)==true)
		{
			left_intake_motor.set(-1);
			right_intake_motor.set(-1);
			//get the current elevator position before we start teleOp
			current_elevator_position = elevatorEncoder.getDistance();
		}
		else if(left_joy_stick.getRawButton(4)==true)
		{
			
			box_grabber.set(DoubleSolenoid.Value.kReverse);
			//get the current elevator position before we start teleOp
			current_elevator_position = elevatorEncoder.getDistance();
		}
		else 
		{
			left_intake_motor.set(0);
			right_intake_motor.set(0); 
		}
		
		if(elevator_joy_stick.getRawButton(2)==true)
		{
			
			compressor.setClosedLoopControl(true);
		}
		else if(elevator_joy_stick.getRawButton(1)==true)
		{
			
			compressor.setClosedLoopControl(false);
		}
		robotvoltage = pdp.getVoltage();
		rightfrontmotor_amps = pdp.getCurrent(2); 
		leftfrontmotor_amps= pdp.getCurrent(12); 
		rightrearmotor_amps= pdp.getCurrent(3); 
		leftrearmotor_amps= pdp.getCurrent(13); 
		elevatormotor_amps= pdp.getCurrent(14); 
		rightintakemotor_amps= pdp.getCurrent(0); 
		leftintakemotor_amps = pdp.getCurrent(1); 
			
		//print the angle every tenth iteration
        if(print_counter % 50 == 0)
        {
        	System.out.printf("Elevator in TeleOp: %f\n", elevator_distance_traveled);
        	System.out.printf("Right Encoder in TeleOp: %f\n", right_wheel_distance_traveled);
        	System.out.printf("Left Encoder in TeleOp: %f\n", left_wheel_distance_traveled);
        	System.out.printf("Gyro in teleOp: %f\n", angle);
        	System.out.printf("Right front motor_amps in teleOp: %f\n", rightfrontmotor_amps);
        	System.out.printf("Left front motor_amps: %f\n", leftfrontmotor_amps);
        	System.out.printf("Right rear motor_amps in teleOp: %f\n", rightrearmotor_amps);
        	System.out.printf("Left rear motor_amps in teleOp: %f\n", leftrearmotor_amps);
        	System.out.printf("Elevator motor_amps in teleOp: %f\n", elevatormotor_amps);
        	System.out.printf("Right intake motor_amps in teleOp: %f\n", rightintakemotor_amps);
        	System.out.printf("Lef intake motor_amps in teleOp: %f\n", leftintakemotor_amps);
        	System.out.printf("Rebot voltage in teleOp: %f\n", robotvoltage);
        }
        //increment
		print_counter++;
	}
	

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testPeriodic() {
		LiveWindow.run();
	}
	
	  @Override
	  /* This function is invoked periodically by the PID Controller, */
	  /* based upon navX-MXP yaw angle input and PID Coefficients.    */
	  public void pidWrite(double output) {
	      rotateToAngleRate = output;
	}
}

/* CODE TO STAY PUT
 * 	//reset the encoder before starting
	leftEncoder.reset();
	rightEncoder.set();
	
	while(isAutonomous())
	{
		//Get the distances each wheel has traveled
		left_wheel_distance_traveled = leftEncoder.getDistance();
		right_wheel_distance_traveled = rightEncoder.getDistance();
		
		robot_drive.arcadeDrive((left_wheel_distance_traveled > right_wheel_distance_traveled ? left_wheel_distance_traveled : right_wheel_distance_traveled)*-0.20 , 0.0);
	}
END CODE TO STAY PUT */

